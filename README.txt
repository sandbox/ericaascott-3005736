Appnovation Managed Services - General Data Protection Regulation (GDPR)
========================================================================

The appno_ms_gdpr module was created to help Appnovation MS developers
to sanitize Personal Identifiable Data (PID) from production databases
as per the requirements under the new General Data Protection Regulation
(GDPR) laws.

INSTALLATION & USAGE INSTRUCTIONS
========================================================================

1. Download and enable module
2. Navigate to /admin/people/appno_ms_gdpr/settings to change config
OR run the drush appno-ms-gdpr-users and drush appno-ms-gdpr-null-fields
commands to set config before running sanitization.
3. Navigate to /admin/people/appno_ms_gdpr to check/run sanitization
OR run drush appno-ms-gdpr-sanitize.
