<?php

/**
 * @file
 * Admin file for appno_ms_gdpr.
 */

/**
 * GDPR Info Page.
 */
function appno_ms_gdpr_info_test_sanitize_form($form, &$form_state) {
  $form['test'] = array(
    '#markup' => '<div style="font-size: 18px; line-height: 25px;">The purpose of this module is to help Data Processors make sure they are adhering to General Data Protection Regulation (GDPR) standards. This module is not a legal guarantee but can help developers sanitize data from production databases so that no Personally Identifiable Data (PID) is accessible. For more information about the laws and regulations, please visit the website here: <a href="https://eugdpr.org/">https://eugdpr.org/</a></div>',
  );

  $sanitized = appno_ms_gdpr_check_for_sanitization();
  if ($sanitized) {
    drupal_set_message(t('Your database has been properly sanitized and no longer has any Personally Identifiable Data (PID)'));
  }
  else {
    drupal_set_message(t('Your database has not been sanitized because it still contains Personally Identifiable Data (PID) from some or all of the users'), 'warning');
    $form['sanitize'] = array(
      '#type' => 'submit',
      '#value' => 'Click Here to Sanitize',
    );
  }

  return $form;
}

/**
 * Submit function for appno_ms_gdpr_info_test_sanitize_form.
 */
function appno_ms_gdpr_info_test_sanitize_form_submit($form, &$form_state) {
  $errors = appno_ms_gdpr_sanitize();
  if (!empty($errors)) {
    drupal_set_message(t('There was an error while sanitizing your database'), 'error');
  }
}

/**
 * GDPR Settings Page.
 */
function appno_ms_gdpr_settings_form($form, &$form_state) {
  $form['table_list_title'] = array(
    '#markup' => '<h1>Non-User Tables to Sanitize</h1>',
  );

  $form['table_list'] = array(
    '#type' => 'textarea',
    '#description' => t('Enter a comma seperated list of non-user tables you would like to sanitize'),
    '#default_value' => appno_ms_gdpr_get_table_list(),
  );

  $form['users_list_title'] = array(
    '#markup' => '<h1>Users Not to Sanitize</h1>',
  );

  $form['users_list'] = array(
    '#type' => 'textarea',
    '#description' => t('Enter a comma seperated list of usernames that you do not want to be sanitized'),
    '#default_value' => appno_ms_gdpr_get_users_to_not_sanitize(),
  );

  $form['must_be_null_title'] = array(
    '#markup' => '<h1>Must Be Null Field Types</h1>',
  );

  $form['must_be_null'] = array(
    '#type' => 'textarea',
    '#description' => t('Enter a comma seperated list of field types that must be null (these are usually images or other complicated fields)'),
    '#default_value' => implode(', ', appno_ms_gdpr_get_must_be_null_fields()),
  );

  $form['table_title'] = array(
    '#markup' => '<h1>User Field Defaults</h1>',
  );

  $form['table'] = array(
    '#tree' => TRUE,
    '#theme' => 'appno_ms_gdpr_settings_table',
  );

  $form['table']['mail']['default'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('appno_ms_gdpr_default_mail', 'test@mailinator.com'),
    '#extra_data' => array('field_name' => 'mail', 'field_type' => 'basic'),
  );

  $form['table']['mail']['default_null'] = array(
    '#type' => 'checkbox',
    '#default_value' => FALSE,
    '#value' => FALSE,
    '#attributes' => array('disabled' => TRUE),
    '#suffix' => 'This field cannot be NULL',
  );

  $pid_fields = appno_ms_gdpr_get_pid_fields();

  foreach ($pid_fields as $pid_field) {
    $cant_be_null = FALSE;
    $must_be_null = in_array($pid_field['field_type'], appno_ms_gdpr_get_must_be_null_fields());
    foreach ($pid_field['field_columns'] as $column) {
      if ($column['not null']) {
        $cant_be_null = TRUE;
      }
    }
    if ($must_be_null && !$cant_be_null) {
      $form['table'][$pid_field['field_name']]['default'] = array(
        '#markup' => 'This field must be NULL',
      );
    }
    elseif (isset($pid_field['field_allowed_values'])) {
      $form['table'][$pid_field['field_name']]['default'] = array(
        '#type' => 'select',
        '#options' => $pid_field['field_allowed_values'],
      );
    }
    else {
      $form['table'][$pid_field['field_name']]['default'] = array(
        '#type' => 'textfield',
      );
    }
    $form['table'][$pid_field['field_name']]['default']['#default_value'] = $pid_field['field_default'];
    $form['table'][$pid_field['field_name']]['default']['#extra_data'] = array('field_name' => $pid_field['field_name'], 'field_type' => $pid_field['field_type']);

    if ($cant_be_null) {
      $form['table'][$pid_field['field_name']]['default_null'] = array(
        '#type' => 'checkbox',
        '#default_value' => FALSE,
        '#value' => FALSE,
        '#attributes' => array('disabled' => TRUE),
        '#suffix' => 'This field cannot be null',
      );
    }
    else {
      $form['table'][$pid_field['field_name']]['default_null'] = array(
        '#type' => 'checkbox',
        '#default_value' => ($must_be_null) ? TRUE : $pid_field['field_default_null'],
      );
      if ($must_be_null) {
        $form['table'][$pid_field['field_name']]['default_null']['#attributes']['disabled'] = TRUE;
        $form['table'][$pid_field['field_name']]['default_null']['#value'] = TRUE;
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save All Values',
  );

  return $form;
}

/**
 * Validate function for appno_ms_gdpr_settings_form.
 */
function appno_ms_gdpr_settings_form_validate($form, &$form_state) {
  $users_list_string = $form_state['values']['users_list'];
  $users_list = explode(', ', $users_list_string);
  foreach ($users_list as $username) {
    if (!user_load_by_name($username)) {
      form_set_error('users_list', t('You have included a username that does not exist'));
    }
  }
}

/**
 * Submit function for appno_ms_gdpr_settings_form.
 */
function appno_ms_gdpr_settings_form_submit($form, &$form_state) {
  $table_values = $form_state['values']['table'];
  foreach ($table_values as $field_name => $table_value) {
    if (isset($table_value['default'])) {
      variable_set('appno_ms_gdpr_default_' . $field_name, $table_value['default']);
    }
    else {
      variable_set('appno_ms_gdpr_default_' . $field_name, NULL);
    }
    variable_set('appno_ms_gdpr_default_null_' . $field_name, $table_value['default_null']);
  }
  $tables_list_string = $form_state['values']['table_list'];
  appno_ms_gdpr_set_table_list($tables_list_string);
  $users_list_string = $form_state['values']['users_list'];
  appno_ms_gdpr_set_users_to_not_sanitize($users_list_string);
  $must_be_null_string = $form_state['values']['must_be_null'];
  appno_ms_gdpr_set_must_be_null_fields($must_be_null_string);
}
