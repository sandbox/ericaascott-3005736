<?php
/**
 * @file
 * Drush file for appno_ms_gdpr module.
 */

/**
 * Drush command to sanitize POD from database.
 */
function drush_appno_ms_gdpr_sanitize() {
  $errors = appno_ms_gdpr_sanitize_users();
  if (!empty($errors)) {
    drupal_set_message(t('There was an error while sanitizing your users'), 'error');
  }
  else {
    drupal_set_message(t('All users have been succesfully sanitized!'));
  }
}

/**
 * Drush command to set users we don't want to sanitize.
 */
function drush_appno_ms_gdpr_users($users = NULL) {
  $user_data_type = drush_get_option('type', NULL);

  switch ($user_data_type) {
    case 'mail':
    case 'uid':
      $users_array = explode(' ', $users);
      $query = db_select('users', 'u')
        ->fields('u', array('name'))
        ->condition($user_data_type, $users_array, 'IN');
      $result = $query->execute()->fetchAll();
      $users_list_string = '';
      foreach ($result as $item) {
        if (empty($users_list_string)) {
          $users_list_string = $item->name;
        }
        else {
          $users_list_string .= ', ' . $item->name;
        }
      }
      break;
    case 'username':
    default:
      $users_list_string = $users;
      break;
  }
  appno_ms_gdpr_set_users_to_not_sanitize($users_list_string);
}

/**
 * Drush command to set fields that must be null.
 */
function drush_appno_ms_gdpr_null_fields($fields = NULL) {
  $fields_array = explode(' ', $fields);
  $fields_string = implode(', ', $fields_array);
  appno_ms_gdpr_set_must_be_null_fields($fields_string);
}

/**
 * Drush command to set tables that must be sanitized.
 */
function drush_appno_ms_gdpr_tables($tables = NULL) {
  $tables_array = explode(' ', $tables);
  $tables_string = implode(', ', $tables_array);
  appno_ms_gdpr_set_table_list($tables_string);
}
